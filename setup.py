from setuptools import setup, find_packages
import os

here = os.path.abspath(os.path.dirname(__file__))

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

# Read dependencies from environment.yml
with open(os.path.join(here, 'environment.yml'), encoding='utf-8') as f:
    # We need to parse without yaml parser, since this runs before any packages are installed
    install_requires = []
    started = False
    for line in f:
        if started:
            install_requires.append(line.strip().replace('- ', ''))
        if 'pip:' in line:
            started = True
    if not install_requires:
        raise ValueError(f'Error parsing pip dependencies from environment.yml')

setup(
    name='abnativ',
    version='0.3.0',
    description='AbNatiV: a VQ-VAE-based assessment of the nativeness of antibodies.',
    licence='MIT licence',
    author='Aubin Ramon',
    author_email='ar2033@cam.ac.uk',
    long_description=long_description,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'abnativ=abnativ.__main__:main'
        ]
    },
    include_package_data=True,
    package_data={'': ["pssms/*.npy"]},
    install_requires=install_requires
)
