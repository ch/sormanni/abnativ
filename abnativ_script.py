#!/usr/bin/env python
import argparse
import sys
from abnativ import scoring
from abnativ import training
from abnativ import vhh_humanisation

USAGE = """

%(prog)s <command> [options]

AbNatiV provides two commands:
    - train: train AbNatiV on a new input dataset of antibody sequences
    - score: use a trained AbNatiV model (default or custom) to score a set of input antibody sequences
    - hum_vhh: humanise a VHH sequence combining AbNatiV VH and VHH assessments

see also 
%(prog)s <command> score -h
%(prog)s <command> train -h
%(prog)s <command> hum_vhh -h
for additional help
"""


def main():

    if len(sys.argv) == 1:
        empty_parser = argparse.ArgumentParser(
            description="VQ-VAE-based assessment of antibody and nanobody nativeness for engineering, selection, and computational design",
            usage=USAGE
        )
        empty_parser.print_help(sys.stderr)
        sys.exit(1)

    parser = argparse.ArgumentParser(
        description="VQ-VAE-based assessment of antibody and nanobody nativeness for engineering, selection, and computational design",
        usage=USAGE
    )

    subparser = parser.add_subparsers()

    # TRAIN
    train_parser = subparser.add_parser("train", 
                                        description="Train AbNatiV on a new input dataset of antibody sequences", 
                                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    train_parser.add_argument('-tr', '--train_filepath', help='Filepath to fasta file .fa with sequences for training', type=str,
                        default='train_2M.fa')

    train_parser.add_argument('-va', '--val_filepath', help='Filepath to fasta file .fa with sequences for validation', type=str,
                        default='val_50k.fa')

    train_parser.add_argument('-hp', '--hparams', help='Filepath to the hyperparameter dictionary .yml', type=str,
                        default='hparams.yml')

    train_parser.add_argument('-mn', '--model_name', help='Name of the model to save checkpoints in', type=str,
                        default='abnativ_v2')

    train_parser.add_argument('-rn', '--run_name', help='Name of the run to log in Pytorch Lightning', type=str,
                        default='abnativ_v2')
    
    train_parser.add_argument('-align', '--do_align', help='Do the alignment and the cleaning of the given sequences before training. \
                              This step can takes a lot of time if the number of sequences is huge.', action="store_true")

    train_parser.add_argument('-isVHH', '--is_VHH', help='Considers the VHH seed for the alignment/ \
                              It is more suitable when aligning nanobody sequences', action="store_true")
    
    train_parser.set_defaults(func=lambda args: training.run(args))

    # SCORE
    score_parser = subparser.add_parser("score", description="Use a trained AbNatiV model (default or custom) to score a set of input antibody sequences", 
                                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    score_parser.add_argument('-nat', '--nativeness_type', help='To load the AbNatiV default trained models type VH, VKappa, VLambda, or VHH, \
                              otherwise add directly the path to your own AbNatiV trained checkpoint .ckpt', type=str,
                                default='VH')

    score_parser.add_argument('-mean', '--mean_score_only', help='Generate only a file with a score per sequence. \
                              If not, generate a second file with a nativeness score per position with a probability score \
                              for each aa at each position.', action="store_true")

    score_parser.add_argument('-i', '--input_filepath_or_seq', help='Filepath to the fasta file .fa to score or directly \
                              a single string sequence', type=str,
                              default='to_score.fa')

    score_parser.add_argument('-odir', '--output_directory', help='Filepath of the folder where all files are saved', type=str,
                              default='abnativ_scoring')

    score_parser.add_argument('-oid', '--output_id', help='Prefix of all the saved filenames', type=str,
                              default='antibody_vh')
    
    score_parser.add_argument('-align', '--do_align', help='Do the alignment and the cleaning of the given sequences before scoring. \
                              This step can takes a lot of time if the number of sequences is huge.', action="store_true")
    
    score_parser.add_argument('-ncpu','--ncpu', help='number of cpu to use for AHo alignment (meaningful if running many sequences)', type=int, default=1)

    score_parser.add_argument('-isVHH', '--is_VHH', help='Considers the VHH seed for the alignment. \
                              It is more suitable when aligning nanobody sequences', action="store_true")
    
    score_parser.add_argument('-plot', '--is_plotting_profiles', help='Plot profile for every input sequence and save them in {output_directory}/{output_id}_profiles.', action="store_true")

    score_parser.add_argument('-v', '--verbose', help='Print more details about every step.', action="store_true")

    score_parser.set_defaults(func=lambda args: scoring.run(args))

    # HUMANISATION 
    hum_parser = subparser.add_parser("hum_vhh", description="Humanise a VHH sequence combining AbNatiV VH and VHH assessments", 
                                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    hum_parser.add_argument('-i', '--input_filepath_or_seq', help='Filepath to the fasta file .fa to score or directly \
                              a single string sequence', type=str,
                              default='to_score.fa')
    
    hum_parser.add_argument('-odir', '--output_directory', help='Filepath of the folder where all files are saved', type=str,
                              default='abnativ_scoring')

    hum_parser.add_argument('-oid', '--output_id', help='Prefix of all the saved filenames (e.g., name sequence)', type=str,
                              default='antibody_vh')
    
    hum_parser.add_argument('-pdb', '--pdb_file', help='Filepath to a pdb crystal structure of the nanobody of interest used to compute \
                                the solvent exposure. If the PDB is not very cleaned that might lead to some false results (which should be flagged byt the \
                                program). If None, will predict the structure using NanoBuilder2', type=str,
                              default=None)
    
    hum_parser.add_argument('-ch', '--ch_id', help='PDB chain id of the nanobody of interest. If -pdb is None, it does not matter', type=str,
                              default='H')
    
    hum_parser.add_argument('-VHscore', '--threshold_abnativ_score', help='Bellow the AbNatiV VH threshold score, a position is considered as a liability', 
                              type=float, default=0.98)
    
    hum_parser.add_argument('-rasa', '--threshold_rasa_score', help='Above this threshold, the residue is considered solvent exposed and is considered for mutation', 
                              type=float, default=0.15)
    
    hum_parser.add_argument('-VHHdecrease', '--perc_allowed_decrease_vhh', help='Maximun ΔVHH score decrease allowed for a mutation', 
                              type=float, default=0.98)
    
    hum_parser.add_argument('-isBrute', '--is_brute', help='If True, runs brute method rather than the enhanced sampling method', action="store_true")

    hum_parser.add_argument('-a', '--a', help='Used for enhanced sampling method in multi-objective selection function: aΔVH+bΔVHH', 
                              type=float, default=0.8)
    
    hum_parser.add_argument('-b', '--b', help='Used for enhanced sampling method in multi-objective selection function: aΔVH+bΔVHH', 
                              type=float, default=0.2)
    
    hum_parser.add_argument('-seq_ref', '--seq_ref', help='If None, does not plot any references in the profiles. \
                            If str, will plot it as a reference (REF) in the comparative WT vs HUM profiles', type=str,
                              default='EVQLVESGGGLVQPGGSLRLSCAASGRTFSYNPMGWFRQAPGKGRELVAAISRTGGSTYYPDSVEGRFTISRDNAKRMVYLQMNSLRAEDTAVYYCAAAGVRAEDGRVRTLPSEYTFWGQGTQVTVSS')

    hum_parser.add_argument('-name_ref', '--name_ref', help='Name of the sequence used a reference for plotting. \
                            If -seq_ref is None, it does not matter', type=str,
                              default='Caplacizumab')

    hum_parser.set_defaults(func=lambda args: vhh_humanisation.run(args))


    args = parser.parse_args()
    args.func(args)

if __name__ == "__main__":
    main()

