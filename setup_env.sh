#!/bin/bash
# Copyright 2024 oliver
#
# MIT License
#

set -e
set -x

function info {
    echo -e "\033[0;32m$1\033[0m"
}

function warn {
    echo -e "\033[0;33m$1\033[0m"
}

# Conda stuff
eval "$(conda shell.bash hook)"
conda activate base

# Check if abnativ environment exists
if conda env list | grep -q /abnativ$; then
    warn "abnativ environment already exists"
    warn "Would you like to remove it and reinstall? [y/N]"
    read -r response
    if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]; then
        conda env remove -n abnativ
    else
        info "Exiting"
        exit 0
    fi
fi

HMMER_BIN_PATH=""

# Check if HMMER is already installed
if command -v hmmbuild &> /dev/null; then
    HMMER_BIN_PATH=$(command -v hmmbuild)
fi

# Check if x86 or arm64
ARCH=$(uname -m)
OS=$(uname -s)

# Check if mamaba is installed
if command -v mamba &> /dev/null; then
    info "Using mamba for faster environment creation"
    CONDA_BIN="mamba"
    # Install mamba hooks
    mamba init &> /dev/null
else
    CONDA_BIN="conda"
fi

if [[ $(uname -m) == "x86_64" ]]; then
    info "Installing abnativ environment for x86_64"
    $CONDA_BIN env create -f environment-x86_64.yml
    $CONDA_BIN activate abnativ
    info "Installing PIP packages (via Poetry)"
    export LD_LIBRARY_PATH=$CONDA_PREFIX/lib:$LD_LIBRARY_PATH
    poetry lock
    poetry install

else
    info "Installing abnativ environment for arm64 (assuming Apple Silicon)"
    BREW_INSTALLED=`command -v brew`
    $CONDA_BIN env create -f environment-noarch.yml
    if [[ -z $HMMER_BIN_PATH ]]; then
        if [[ $BREW_INSTALLED ]]; then
            info "Installing HMMER via Homebrew"
            brew install hmmer
        else
            warn "Homebrew not installed. Please install homebrew and run the script again"
            exit 1
        fi
    else
        info "HMMER already installed at $HMMER_BIN_PATH"
    fi

    info "Activating environment"
    conda activate abnativ

    info "Installing PIP packages (via Poetry)"
    poetry lock
    poetry install

    mkdir -p $HOME/.abnativ
    cd $HOME/.abnativ

    # Manually install ANARCI
    if [[ -d "$HOME/.abnativ/ANARCI" ]]; then
        warn "ANARCI already located at $HOME/.abnativ/ANARCI"
        cd "$HOME/.abnativ/ANARCI"
        info "Updating ANARCI (from github)"
        git pull
        cd -
    else
        info "Installing ANARCI (from github)"
        git clone https://github.com/oxpig/ANARCI.git
    fi

    cd "$HOME/.abnativ/ANARCI"
    info "Installing ANARCI (this may take a while)"
    python setup.py install > /dev/null
    cd -
fi

info "To run abnativ, activate the environment with:"
info "$ conda activate abnativ"
info "$ abnativ"

info "Downloading the pretrained models with abnativ update"
abnativ update 
